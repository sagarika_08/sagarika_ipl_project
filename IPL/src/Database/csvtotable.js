const fs = require("fs");
const fastCsv = require("fast-csv");
const dbConnection = require("../client/mysql_Connection");

function createTable() {
  let con = dbConnection();
  con.connect((error) => {
    if (error) console.log(error);
    else {
      let matchTableCreation = `CREATE TABLE IF NOT EXISTS matches(id INT PRIMARY KEY,season INT NOT NULL,city TEXT NOT NULL,date DATE NOT NULL,
        team1 TEXT NOT NULL,team2 TEXT NOT NULL,toss_winner TEXT NOT NULL,toss_decision TEXT NOT NULL,result TEXT NOT NULL,dl_applied INT,    
        winner TEXT NOT NULL,win_by_runs INT NOT NULL,win_by_wickets INT NOT NULL,player_of_match TEXT NOT NULL,  venue TEXT NOT NULL,
        umpire1 TEXT NOT NULL,umpire2 TEXT NOT NULL,umpire3 TEXT NOT NULL)`;
      con.query(matchTableCreation, (err, data) => {
        if (err)
          throw err;
        console.log('match table created')
      })
      let deliveriesTableCreation = `CREATE TABLE IF NOT EXISTS deliveries (match_id INT,inning INT NOT NULL,batting_team TEXT NOT NULL,  
        bowling_team TEXT NOT NULL,overs INT NOT NULL,ball INT NOT NULL,batsman TEXT NOT NULL,non_striker TEXT NOT NULL, bowler TEXT NOT NULL,
        is_super_over INT NOT NULL,wide_runs INT NOT NULL, bye_runs INT NOT NULL, legbye_runs INT NOT NULL, noball_runs INT NOT NULL,
        penalty_runs INT NOT NULL,batsman_runs INT NOT NULL,extra_runs INT NOT NULL,  total_runs INT NOT NULL,player_dismissed TEXT ,
        dismissal_kind TEXT,fielder TEXT,FOREIGN KEY (match_id) REFERENCES matches(id) ,
        PRIMARY KEY (match_id,batting_team,bowling_team,overs,ball,batsman,non_striker,bowler))`;
      con.query(deliveriesTableCreation, (err, data) => {
        if (err)
          throw err;
        console.log('deliveries table created')
      })
    }
  })
}


function insertion(filename) {
  let csvData = [];
  let csvStream = fastCsv.parse()
  fs.createReadStream("../data/" + filename)
    .pipe(csvStream)
    .on("error", (error) => {
        console.error(error)
    })
    .on("data", (row) => {
        csvData.push(row);
    })
    .on("end", () => {
      let tableName = filename.split(".")[0];
      csvData.shift();
      let con = dbConnection();
      con.connect((err) => {
        if (err) console.log(error);
        else {
          let query = `insert into ${tableName} values ?`;
          con.query(query, [csvData], (err, response) => {
            if (err) console.log(err);
            else console.log(`Data inserted successfully to ${tableName}`);
            con.end();
          });
        }
      });
    });
}



// createTable()
// insertion("matches.csv");
// insertion("deliveries.csv");

