const dbConnection = require("../Database/mysql_Connection");

function noOfMatchPerYear() {
  let resultMatchesPerYear = [];
  return new Promise((resolve, reject) => {
    let con = dbConnection();
    con.connect((error) => {
      if (error) console.log(error);
      else {
        let query1 = "SELECT season,COUNT(id) as no_of_matches FROM matches group by season order by season asc";
        con.query(query1, (err, data) => {
          if (err) reject(err);
          else {
            console.log("data from query1", data)
            data.forEach((ele) => {
              resultMatchesPerYear.push([ele.season, ele.no_of_matches]);
            })
          }
          con.end();
          resolve(JSON.stringify(resultMatchesPerYear));
        })
      }
    })
  })
}
function noOfMatchesWonPerTeamYear() { 

  let resultMatchesWonPerTeamYear = {};
  return new Promise((resolve,reject)=>{
    let con = dbConnection();
    con.connect((error)=>{
      if(error) console.log(error)
      else{
        let query3="select season,winner,count(winner) as matches from matches group by season,winner order by season";
          con.query(query3,(err,data)=>{
          if(err) reject (err);
          else{
            console.log("data from query1", data)
            data.forEach((ele) => {
              if (ele.season in resultMatchesWonPerTeamYear) {
                resultMatchesWonPerTeamYear[ele.season][ele.winner] = ele.matches;
              } else {
                resultMatchesWonPerTeamYear[ele.season] = { [ele.winner]: ele.matches };
              } 
            })
          }
          con.end();
          resolve(JSON.stringify(resultMatchesWonPerTeamYear));
        })
      }
    
    })
  })



}
function extraRunConcededPerTeam() { 
  let resultExtraRunConcededPerTeam = [];
  return new Promise((resolve,reject)=>{
    let con = dbConnection();
    con.connect((error)=>{
      if(error) console.log(error)
      else{
        let query2="select bowling_team,sum(extra_runs) as extra_runs from deliveries where match_id in (select id from matches where season = 2016) group by bowling_team";
        con.query(query2,(err,data)=>{
          if(err) reject (err);
          else{
            console.log("data from query1", data)
            data.forEach((ele) => {
              resultExtraRunConcededPerTeam.push([ele.bowling_team, ele.extra_runs]);
            })
          }
          con.end();
          resolve(JSON.stringify(resultExtraRunConcededPerTeam));
        })
      }
    
    })
  })
}
function top10Bowlers() { 
  let resultTopBowlers = [];
  return new Promise((resolve,reject)=>{
    let con = dbConnection();
    con.connect((error)=>{
      if(error) console.log(error)
      else{
        let query3="select bowler,round( (sum(total_runs)) / (sum(overs)/6)  ,2) as economic_rate from deliveries where match_id in (select id from matches where season = 2015) group by bowler order by economic_rate desc limit 10";
        con.query(query3,(err,data)=>{
          if(err) reject (err);
          else{
            console.log("data from query1", data)
            data.forEach((ele) => {
              resultTopBowlers.push([ele.bowler, ele.economic_rate]);
            })
          }
          con.end();
          resolve(JSON.stringify(resultTopBowlers));
        })
      }
    
    })
  })
}
module.exports = {
  noOfMatchesWonPerTeamYear,
  noOfMatchPerYear,
  extraRunConcededPerTeam,
  top10Bowlers
}