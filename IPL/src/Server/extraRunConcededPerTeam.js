/**This function will return the extra run concede each team in the specified year
 * 
 * @param  {Object} matches contains matches data
 * @param  {Object} delivers contains deliveries data
 * @param  {number} yr The function will the respective result of this year
 * @returns {Object} result return top 10 bowlers with high economic rate
 * 
 */

const extraRunConcededPerTeam = ((matches, delivers, yr) => {
    if (((yr) && (matches) && (delivers)) != undefined && ((yr) && (matches) && (delivers)) != null) {

        /**
         * @type {Object} del_data
         * @type {number} m_id
         */

        let result = {}
        let del_data = {}

        matches.forEach(match => {
            let m_id
            if (match.season == yr) {
                m_id = match.id

                del_data = delivers.filter(deliverData => {
                    if (m_id === deliverData.match_id) return deliverData
                })

                del_data.forEach(data => {
                    if (result[data.bowling_team]) {
                        result[data.bowling_team] = result[data.bowling_team] + parseInt(data.extra_runs)
                    }
                    else result[data.bowling_team] = parseInt(data.extra_runs)

                })
            }
        });

        // console.log(result)

        return result
    }
    else return {}
})

module.exports = extraRunConcededPerTeam
