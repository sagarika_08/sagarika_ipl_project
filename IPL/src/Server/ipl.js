const dataDeliveries = '../data/deliveries.csv'
const dataMatches = '../data/matches.csv'
const noOfMatchesPerYear = require('./noOfMatchesPerYear')
const noOfMatchesWonPerTeamYear =require('./noOfMatchesWonPerTeamYear')
const extraRunConcededPerTeam=require('./extraRunConcededPerTeam')
const top10Bowlers = require('./top10Bowlers')
const csv = require('csvtojson')
const fs = require('fs')
    csv()
    .fromFile(dataMatches)
    .then(match =>{
      //  console.log(match.slice(0,3))
        let numOfMatchesYear = noOfMatchesPerYear(match)
        writeNoOfMatchesPerYear(numOfMatchesYear)
    
        let noOfMatchesWonTeamYear = noOfMatchesWonPerTeamYear(match)
        writeMatchesWonPerTeamYear(noOfMatchesWonTeamYear)  
        return(match)  
    })
    .then(match=>{
        csv()
        .fromFile(dataDeliveries) 
        .then (delivers =>{
            //console.table(delivers.slice(0,20))
            let runConcededPerTeam=extraRunConcededPerTeam(match,delivers,2016)
            writeRunConcededPerTeam(runConcededPerTeam)
          
            let bestBowlers = top10Bowlers(match,delivers,2015)
            writeBestBowlers(bestBowlers)
        })

})    
const writeNoOfMatchesPerYear = (data =>{
        //  const jsonData = {
        //     noOfMatchesPerYear : data
        //  }
         fs.writeFile('../public/output/matchesPerYear.json',JSON.stringify(data),err =>
         {
             if (err) throw err
            console.log("Done Writing to matchesPerYear.json")
         }) 
})
const writeMatchesWonPerTeamYear=(data=>{
    // const jsonData={
    //     noOfMatchesWonPerTeamPerYear : data
    // }
    fs.writeFile('../public/output/matchesWonPerTeamYear.json',JSON.stringify(data),err=>{
        if(err) throw err
        console.log("Done Writing to matchesWonPerTeamYear.json")
    })
})
const writeRunConcededPerTeam =( data =>{
    // const jsonData = {
    //     2016 : data
    // }
    fs.writeFile('../public/output/runConcededPerTeam.json',JSON.stringify(data),err=>{
        if(err) throw err;
        console.log("Done Writing to runConcededPerTeam.json")
    })
})
const writeBestBowlers =( data =>{
    // const jsonData = {
    //     2015 : data
    // }
    fs.writeFile('../public/output/top10Bowlers.json',JSON.stringify(data),err=>{
        if(err) throw err;
        console.log("Done Writing to top10Bowlers.json")
    })
})