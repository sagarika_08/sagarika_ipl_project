/**This function will return top ten bowlers according to their economic rate in the specified year
 * @param  {Object} matches contains matches data
 * @param  {Object} deliveries contains deliveries data
 * @param  {number} yr The function will the respective result of this year
 * @returns {Object} BowlerResult return top 10 bowlers with high economic rate
 */

const top10Bowlers = ((matches, deliveries, yr) => {
    if (((yr) && (matches) && (deliveries)) != undefined && ((yr) && (matches) && (deliveries)) != null) {
        /**
         * @type {Object} result
         * @type {Object} matchId
         * @type {Object} deliverId
         */

        const BowlerResult = []
        const result = {}
        const matchId = matches.filter((obj) => obj.season == yr).map(obj => obj.id)
        const deliverId = deliveries.filter((obj) => matchId.includes(obj.match_id))

        deliverId.forEach(delData => {
            const delBowler = delData.bowler
            const delOver =  parseInt(delData.over)
            const delRunConcede = parseInt(delData.total_runs)

            if (result[delBowler]) {
                result[delBowler].runConcede += delRunConcede
                result[delBowler].over += delOver
            }
            else {
                result[delBowler] = {}
                result[delBowler].bowler = delBowler
                result[delBowler].runConcede = delRunConcede
                result[delBowler].over = delOver
            }
        })
        //console.log(result)
        /**
         * @type {number} economic
         */
      
        for (let key in result) {
            let economic = result[key].runConcede / result[key].over
            let inter = {
                bowler: key,
                economicRate: economic.toFixed(2)
            }
            BowlerResult.push(inter)
        }
        //console.log(BowlerResult.sort((a, b) => (a.economicRate < b.economicRate) ? 1 : -1).slice(0, 10))
        return (BowlerResult.sort((a, b) => (a.economicRate < b.economicRate) ? 1 : -1).slice(0, 10))

    }

    else {
        return {}
    }
})
module.exports = top10Bowlers

