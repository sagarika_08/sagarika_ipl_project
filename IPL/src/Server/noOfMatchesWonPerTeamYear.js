/**This function will return the number of matches won per team in each year
 * @param  {Object} matches contains matches data
 * @returns {Object} result  
 */

const noOfMatchesWonPerTeamYear = (matches =>{
/**
 * @type {number} year - storing season(year) value of each match take place
 * @type {string} winner - storing winner each match of input 
 */
    const result = {}
    matches.forEach(match =>{
        const year = match.season
        const winner = match.winner
        if(result[year]){
            if(result[year][winner])
                result[year][winner]+=1
            else
                result[year][winner]=1
        }
        else{
            result[year] ={}
            result[year][winner]=1
        }
    })
    //console.log(result)
    return result
})
module.exports = noOfMatchesWonPerTeamYear;
