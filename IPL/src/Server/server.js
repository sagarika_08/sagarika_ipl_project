const http = require("http");
const fs = require("fs");
const path = require("path");
const db = require("../Database/databaseOperation");
const server = http.createServer((request, response) => {

    const requestUrl = request.url
    console.log(requestUrl);
    const reqUrlExt = requestUrl.split('.').splice(-1)[0]
    const clientFolderPath = path.resolve(__dirname, '../client')
    const outputFolderPath = path.resolve(__dirname, '../public/output')


    switch (reqUrlExt) {

        case '/':

            let htmlPath = path.resolve(__dirname, '../client/index.html')
            fs.readFile(htmlPath, 'utf-8', (err, data) => {
                if (err) {
                    response.writeHead(404)
                    response.end(err)
                }
                else {
                    response.writeHead(200, { 'Content-Type': 'text/html' })
                    response.end(data)
                }
            })
            break;
        case 'json':
            let jsonPath = path.join(outputFolderPath, requestUrl)
            fs.readFile(jsonPath, 'utf-8', (err, data) => {
                if (err) {
                    response.writeHead(404)
                    response.end(err)
                }
                else {
                    response.writeHead(200, { 'Content-Type': 'application/json' })
                    response.end(data)
                }
            })
            break;
        case 'js':
            let jsFilePath = path.join(clientFolderPath, requestUrl)
            fs.readFile(jsFilePath, 'utf-8', (err, data) => {
                if (err) {
                    response.writeHead(404)
                    response.end(err)
                }
                else {
                    response.writeHead(200, { 'Content-Type': 'application/js' })
                    response.end(data)
                }
            })
            break;
        case 'css':
            const cssPath = path.join(clientFolderPath, requestUrl)
            fs.readFile(cssPath, 'utf-8', (err, data) => {
                if (err) {
                    response.writeHead(404)
                    response.end(err)
                }
                else {
                    response.writeHead(200, { 'Content-Type': 'text/css' })
                    response.end(data)
                }
            })
            break
        

    }
    switch (requestUrl){
        case "/noOfMatchPerYear":
            db
                .noOfMatchPerYear()
                .then((data)=>{
                    response.writeHead(200,{"content-Type":"application/json"});
                    response.end(data);
                })
                .catch((err)=>{
                    console.log(err);
                    response.writeHead(404);
                    response.end(err);
                })
            break;
        case "/extraRunConcededPerTeam":
            db
                .extraRunConcededPerTeam()
                .then((data) => {
                    response.writeHead(200, { "Content-Type": "application/json" });
                    response.end(data);
                })
                .catch((err) => {
                    console.log(err);
                    response.writeHead(404);
                    response.end(JSON.stringify(err));
                });

            break;
        case "/top10Bowlers":
            db
            .top10Bowlers()
            .then((data) => {
                response.writeHead(200, { "Content-Type": "application/json" });
                response.end(data);
            })
            .catch((err) => {
                console.log(err);
                response.writeHead(404);
                response.end(JSON.stringify(err));
            });

            break;
        case "/noOfMatchesWonPerTeamYear":
            db
            .noOfMatchesWonPerTeamYear()
            .then((data) => {
                response.writeHead(200, { "Content-Type": "application/json" });
                response.end(data);
            })
            .catch((err) => {
                console.log(err);
                response.writeHead(404);
                response.end(JSON.stringify(err));
            });

            break;

    }
})
server.listen(3000, (err) => {
    if (err) console.log('error in server creation', err)
    else console.log('server ready')
})





