/**
 * This function is used to count out no of matches took place in each year.
 * It will return the result a object
 * @param  {Object} matches - It is a json object containing matches data
 * @return  {Object}  matchesPerYear result is return back
 */

 
const noOfMatchesPerYear = (matches => {
    const matchesPerYear = {}
    /**
     * @type {number} year - storing season(year) value of each match take place
     */
    matches.forEach(match => {
        const season = match.season;
        if (matchesPerYear.hasOwnProperty(season) == true) {
            matchesPerYear[season] += 1;
        }
        else {
            matchesPerYear[season] = 1;
        }
    })
    //console.log(matchesPerYear)
    return matchesPerYear;

})
module.exports = noOfMatchesPerYear;
