const fetchData = function () {

    let noOfMatchPerYear = []
    fetch("http://localhost:3000/noOfMatchPerYear")
    .then(function (response){
      response.json().then(function (data){
        console.log("returned data of noOfMatchPerYear");
        console.log(data)
        shownOnOfMatchPerYear(data);
      })
    })
    .catch(err=>{
      console.log(err);
    })
    fetch("http://localhost:3000/extraRunConcededPerTeam")
    .then(function (response){
      response.json().then(function (data){
        console.log('extraRunConcededPerTeam')
        console.log(data)
        showExtraRunConcededPerTeam(data)
      })
    })
    .catch(err=>{
      console.log(err);
    })
    fetch("http://localhost:3000/top10Bowlers")
    .then(function (response){
      response.json().then(function (data){
        console.log('top10Bowlers')
        console.log(data)
        showTop10Bowlers(data)
      })
    })
    .catch(err=>{
      console.log(err);
    })
    fetch("http://localhost:3000/noOfMatchesWonPerTeamYear")
    .then(function (response){
      response.json().then(function (data){
        window.resultOfMatchesWonPerTeamYear=data;
        console.log('noOfMatchesWonPerTeamYear')
        console.log(data)
        showNoOfMatchesWonPerTeamYear()
      })
    })
    .catch(err=>{
      console.log(err);
    })
  };






  fetchData();
  
  function shownOnOfMatchPerYear(data){

    Highcharts.chart('plots1', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Year'
            },
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Match Played'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
        },
        series: [{
            name: 'Match Played',
            data: data,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

  }
  function showExtraRunConcededPerTeam(data){
    Highcharts.chart('plots2', {
      chart: {
          type: 'column'
      },
      title: {
          text: ''
      },
      xAxis: {
          type: 'category',
          title: {
              text: 'Team'
          },
          labels: {
              rotation: -45,
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              },
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Run Conceded'
          }
      },
      legend: {
          enabled: false
      },
      tooltip: {
          pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
      },
      series: [{
          name: 'Match Played',
          data: data,
          dataLabels: {
              enabled: true,
              rotation: -90,
              color: '#FFFFFF',
              align: 'right',
              format: '{point.y:f}', // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      }]
  });
  }
  function showTop10Bowlers(data){
    Highcharts.chart('plots3', {
      chart: {
          type: 'column'
      },
      title: {
          text: ''
      },
      xAxis: {
          type: 'category',
          title: {
              text: 'bowlers'
          },
          labels: {
              rotation: -45,
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              },
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Economic rate'
          }
      },
      legend: {
          enabled: false
      },
      tooltip: {
          pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
      },
      series: [{
          name: 'Match Played',
          data: data,
          dataLabels: {
              enabled: true,
              rotation: -90,
              color: '#FFFFFF',
              align: 'right',
              format: '{point.y:f}', // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                  fontSize: '13px',
                  fontFamily: 'Verdana, sans-serif'
              }
          }
      }]
  });

  }
  function showNoOfMatchesWonPerTeamYear(){
    let dropdownYear = document.getElementById("matchesWonYearValue")
    let year = dropdownYear.value;
    let matchesWonPerTeamYear = []
    for (let index in resultOfMatchesWonPerTeamYear) {
        if (index == year) {
            for (let wonTeams in resultOfMatchesWonPerTeamYear[index]) {
                if (wonTeams != '')
                    matchesWonPerTeamYear.push([wonTeams, resultOfMatchesWonPerTeamYear[index][wonTeams]])
            }
        }
    }
    Highcharts.chart('plots4', {
        chart: {
            type: 'column'
        },
        title: {
            text: `In ${year}`
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Team'
            },
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No.of Matches won'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
        },
        series: [{
            name: 'Match Played',
            data: matchesWonPerTeamYear,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
  }