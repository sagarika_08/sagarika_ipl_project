const fetchMatchPlayedData = function () {

    fetch('http://localhost:3000/matchesPerYear.json')
        .then(function (response) {
            response.json()
            .then(function (data) {
                matchesPerYearHighChart(data);
                });
        });


    fetch('http://localhost:3000/matchesWonPerTeamYear.json')
        .then(function (response) {
            response.json()
                .then(function (data) {
                    window.matchesWonPerTeamYearData = data
                    matchesWonPerTeamYear()
                })
        });


    fetch('http://localhost:3000/runConcededPerTeam.json')
        .then(function (response) {
            response.json()
            .then(function (data) {
                runConcededPerTeam(data)
            })
        });


    fetch('http://localhost:3000/top10Bowlers.json')
        .then(function (response) {
            response.json()
            .then(function (data) {
                top10Bowlers(data)
            })
        })
}
fetchMatchPlayedData();




function runConcededPerTeam(data) {
    let runConcededPerTeamArr = [];
    for (key in data) {
        // console.log(key + '          '+data[key])
        runConcededPerTeamArr.push([key, data[key]]);
    }
    //  console.log(runConcededPerTeamArr)

    Highcharts.chart('plots2', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Team'
            },
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Run Conceded'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
        },
        series: [{
            name: 'Match Played',
            data: runConcededPerTeamArr,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}


function top10Bowlers(data) {
    let top10BowlersArr = [];
    for (key in data) {
        //    console.log(data[key].bowler + '        ' + data[key].economicRate)
        top10BowlersArr.push(
            [
                data[key].bowler,
                parseFloat(data[key].economicRate)
            ]);
    }
    //   console.log(top10BowlersArr)
    Highcharts.chart('plots3', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'bowlers'
            },
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economic rate'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
        },
        series: [{
            name: 'Match Played',
            data: top10BowlersArr,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });



}

function matchesPerYearHighChart(data) {
    let matchesPerYear = [];
    for (key in data) {
        matchesPerYear.push([key, data[key]]);
    }
    Highcharts.chart('plots1', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Year'
            },
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Match Played'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
        },
        series: [{
            name: 'Match Played',
            data: matchesPerYear,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

}

function matchesWonPerTeamYear() {
    // document.getElementById("matchesWonYearValue")
    let dropdownYear = document.getElementById("matchesWonYearValue")
    let year = dropdownYear.value;


    let matchesWonPerTeamYear = []
    for (let index in matchesWonPerTeamYearData) {
        if (index == year) {
            for (let wonTeams in matchesWonPerTeamYearData[index]) {
                if (wonTeams != '')
                    matchesWonPerTeamYear.push([wonTeams, matchesWonPerTeamYearData[index][wonTeams]])
            }
        }
    }
    //    console.log(year)
    //  console.log(matchesWonPerTeamYear)
    Highcharts.chart('plots4', {
        chart: {
            type: 'column'
        },
        title: {
            text: `In ${year}`
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Team'
            },
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                },
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No.of Matches won'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Match Played : <b>{point.y:.1f} matches</b>'
        },
        series: [{
            name: 'Match Played',
            data: matchesWonPerTeamYear,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });

}




















